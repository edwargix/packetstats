//
// Created by Phil Romig on 11/13/18.
//

#include "packetstats.h"

// ****************************************************************************
// * pk_processor()
// *  Most/all of the work done by the program will be done here (or at least it
// *  it will originate here). The function will be called once for every
// *  packet in the savefile.
// ****************************************************************************
void pk_processor(u_char *user, const struct pcap_pkthdr *pkthdr, const u_char *packet) {

    resultsC* results = (resultsC*)user;
    results->incrementTotalPacketCount();
    DEBUG << "Processing packet #" << results->packetCount() << ENDL;
    char s[256]; bzero(s,256); bcopy(ctime(&(pkthdr->ts.tv_sec)),s,strlen(ctime(&(pkthdr->ts.tv_sec)))-1);
    TRACE << "\tPacket timestamp is " << s;
    TRACE << "\tPacket capture length is " << pkthdr->caplen ;
    TRACE << "\tPacket physical length is " << pkthdr->len ;

    // ***********************************************************************
    // * Process the link layer header
    // *  Hint -> use the ether_header structure defined in
    // ***********************************************************************

    struct ether_header *eth_header = (struct ether_header *) packet;

    uint16_t type = ntohs(eth_header->ether_type);
    if (type < 0x0600) { /* 802.3 packet */
        results->newOtherLink(pkthdr->caplen);

        struct ethhdr *header_802 = (struct ethhdr *) packet;
        uint64_t buf = 0;
        memcpy(&buf, header_802->h_source, sizeof(header_802->h_source));
        results->newSrcMac(buf);
        memcpy(&buf, header_802->h_dest, sizeof(header_802->h_dest));
        results->newDstMac(buf);

        return;
    }

    uint64_t buf = 0;
    memcpy(&buf, eth_header->ether_shost, sizeof(eth_header->ether_shost));
    results->newSrcMac(buf);
    memcpy(&buf, eth_header->ether_dhost, sizeof(eth_header->ether_dhost));
    results->newDstMac(buf);

    results->newEthernet(pkthdr->caplen);

    // ***********************************************************************
    // * Process the network layer
    // ***********************************************************************

    if (type == ETHERTYPE_IPV6) {
        results->newIPv6(pkthdr->caplen);
        return;
    } else if (type == ETHERTYPE_ARP) {
        results->newARP(pkthdr->caplen);
        return;
    } else if (type == ETHERTYPE_IP) {
        results->newIPv4(pkthdr->caplen);
    } else {
        results->newOtherNetwork(pkthdr->caplen);
        return;
    }

    // ***********************************************************************
    // * Process the transport layer header
    // ***********************************************************************

    struct ip *network = (struct ip *) (packet + sizeof(struct ether_header));
    uint8_t protocol = network->ip_p;

    results->newSrcIPv4(network->ip_src.s_addr);
    results->newDstIPv4(network->ip_dst.s_addr);

    if (protocol == IPPROTO_ICMP) {
        results->newICMP(pkthdr->caplen);
        return;
    } else if (protocol == IPPROTO_TCP) {
        results->newTCP(pkthdr->caplen);
        struct tcphdr *transport_tcp = (struct tcphdr *) (((char *) network) + sizeof(struct ip));
        results->newSrcTCP(ntohs(transport_tcp->source));
        results->newDstTCP(ntohs(transport_tcp->dest));

        if (transport_tcp->th_flags & TH_FIN)
            results->incrementFinCount();
        else if (transport_tcp->th_flags & TH_SYN)
            results->incrementSynCount();
    } else if (protocol == IPPROTO_UDP) {
        results->newUDP(pkthdr->caplen);
        struct udphdr *transport_udp = (struct udphdr *) (((char *) network) + sizeof(struct ip));
        results->newSrcUDP(ntohs(transport_udp->source));
        results->newDstUDP(ntohs(transport_udp->dest));
    } else {
        results->newOtherTransport(pkthdr->caplen);
        return;
    }
}
